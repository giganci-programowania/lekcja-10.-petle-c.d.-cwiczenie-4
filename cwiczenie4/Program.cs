﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cwiczenie4
{
    class Program
    {
        // Dwaj koledzy Janek i Karol zadłużyli się na 80 zł. Umówili się między sobą,
        // że każdy z nich codziennie będzie odkładał na spłatę zadłużenia 20% swojego
        // dziennego zarobku, Janek zarabia dziennie 50 zł, a Karol 40 zł.
        // Napisz program, który przy pomocy pętli do..while sprawdzi po ilu dniach obaj
        // koledzy uzbierają kwotę potrzebną do spłaty swojego długu.
        // Zmodyfikuj program, aby wypisywał wartość uzbieranej kwoty w każdym dniu.
        static void Main(string[] args)
        {
            int i = 1;
            float dlug = 80f;
            float zarobekJanka = 50f;
            float zarobekKarola = 40f;
            float splata = 0f;
            do
            {
                splata += 0.2f * zarobekJanka + 0.2f * zarobekKarola;
                Console.WriteLine("Dzień = {0} Spłata = {1}", i, splata);
                i++;
            }
            while (dlug > splata);

            Console.WriteLine("Dług spłacony po= {0} dniach", i);
            Console.ReadKey();
        }
    }
}
